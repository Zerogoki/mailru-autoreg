import requests
from multiprocessing.dummy import Pool as ThreadPool
from datetime import datetime
from utils.proxy_manager import ProxyManager
import sys
from ssl import SSLError
from requests.exceptions import RequestException


def check_proxy(proxy):
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'DNT': '1',
               'Upgrade-Insecure-Requests': '1'}
    proxy_str = '%s:%s' % (proxy[0], proxy[1])
    p = {
        'http': '%s://%s' % (proxy[2], proxy_str),
        'https': '%s://%s' % (proxy[2], proxy_str)
    }
    try:
        resp = requests.get('https://e.mail.ru/signup?from=main_noc', headers=headers, proxies=p, timeout=1.2) \
            .content.decode('utf-8')
        requests.get('https://c.mail.ru/c/2', proxies=p, timeout=4)
        if 'id="noPhoneLink"' in resp:
            print('\033[92m%s:%s is working. MailRu OK\033[0m' % (proxy_str, proxy[2]))  # green output
            return proxy
        else:
            print('%s:%s is working. MailRu FAIL' % (proxy_str, proxy[2]))
            return False
    except (RequestException, SSLError):
        pass


start_time = datetime.now()
proxy_manager = ProxyManager()
if len(sys.argv) == 2:
    proxy_manager.grab_proxies()
    print('Grabbed %d proxies' % proxy_manager.count)
else:
    status = proxy_manager.read_from_file(sys.argv[2])
    if not status:
        print('No such file')
        exit()
pool = ThreadPool(40)
results = pool.map(check_proxy, proxy_manager.proxies)
failed_mail_ru = 0
for elem in results:
    if elem is False:
        failed_mail_ru += 1
good_proxies = [x for x in results if x]
working_proxies = len(good_proxies) + failed_mail_ru
print('%d proxies of %d are working, %d of them are suitable for MailRu' % (working_proxies, proxy_manager.count,
                                                                            len(good_proxies)))
print('Time spent:', datetime.now() - start_time)
proxy_manager.write_to_file(sys.argv[1], good_proxies)
