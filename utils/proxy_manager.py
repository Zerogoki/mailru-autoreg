from importlib.machinery import SourceFileLoader
import os


class ProxyManager:
    def __init__(self):
        self.count = 0
        self.proxies = []
        self.__crawlers__ = []
        files = list(os.walk('crawlers'))[0][2]
        for file in files:
            if file.split('.')[1] == 'py':
                self.__crawlers__.append(SourceFileLoader('c', './crawlers/' + file).load_module().crawler)

    def grab_proxies(self):
        for crawler in self.__crawlers__:
            result = crawler()
            self.proxies += result[0]
            self.count += result[1]

    def read_from_file(self, file_name):
        try:
            with open(file_name, 'r') as f:
                input_proxies = f.read().split()
                if len(input_proxies) == 0:
                    return 1
        except FileNotFoundError:
            return False
        counter = 0
        for i in range(len(input_proxies)):
            input_proxies[i] = input_proxies[i].split(':')
            if len(input_proxies[i]) == 2:
                input_proxies[i].append('http')
            counter += 1
        self.proxies = input_proxies
        self.count = counter
        return True

    def write_to_file(self, file_name, proxies=None):
        if proxies is None:
            proxies = self.proxies
        with open(file_name, 'w') as f:
            s = []
            for p in proxies:
                s.append('%s:%s:%s' % (p[0], p[1], p[2]))
            s = '\n'.join(s)
            f.write(s)
