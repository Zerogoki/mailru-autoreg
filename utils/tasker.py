import random
from requests.exceptions import RequestException
from ssl import SSLError
from utils.mailru import MailRu


class Tasks:
    def __init__(self, proxies):
        self.proxies = proxies
        self.tasks = []
        self.registered = []

    def add_task(self, task):
        self.tasks.append(task)

    def process(self):
        limit = len(self.tasks)
        counter = 0
        while counter < limit:
            if not len(self.proxies):
                self.registered = None
                break
            task = self.tasks[counter]
            p_i = random.randint(0, len(self.proxies) - 1)
            p = self.proxies[p_i]
            print('\nSwitched to %s proxy %s:%s' % (p[2], p[0], p[1]))
            try:
                result = self.reg_acc(task, p)
            except (RequestException, SSLError):
                print('Proxy offline')
                result = False
            if not result:
                del self.proxies[p_i]
                continue
            else:
                if result == 1:
                    print('Bad first or last name')
                    del self.tasks[counter]
                    limit -= 1
                else:
                    print('Success')
                    self.registered.append(result)
                    counter += 1

    def reg_acc(self, tsk, px):
        f_name = tsk[0]
        l_name = tsk[1]
        s = tsk[2]
        mail = MailRu(f_name, l_name, s, proxy=px)
        if mail.status == 1:
            print('MailRu requests phone number')
            return False
        emails = mail.get_email_list()
        if not emails:
            return 1
        s_email = random.choice(emails)
        while True:
            self.write_captcha(mail)
            code = input('Captcha from captcha.html: ')
            r = mail.register(code, s_email)
            status = r[0]
            if not status:
                error_code = r[1]
                if error_code == 1:
                    print('Incorrect captcha')
                    self.write_captcha(mail)
                elif error_code == 2:
                    print('Too many accounts from IP')
                    return False
                continue
            else:
                r_email = r[1]
                r_password = r[2]
                return r_email, r_password

    @staticmethod
    def write_captcha(m):
        with open('captcha.jpg', 'wb') as ff:
            ff.write(m.captcha)
