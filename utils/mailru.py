import requests
import random
import string


class MailRu:
    def __init__(self, f_name, l_name, sex, proxy=None):
        self.__headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
                          'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'DNT': '1',
                          'Upgrade-Insecure-Requests': '1'}
        self.__cookies = {}
        if proxy is None:
            self.__proxies = None
        else:
            self.__proxies = {
                'http': '%s://%s:%s' % (proxy[2], proxy[0], proxy[1]),
                'https': '%s://%s:%s' % (proxy[2], proxy[0], proxy[1])
            }
        resp = requests.get('https://e.mail.ru/signup?from=main_noc', headers=self.__headers, proxies=self.__proxies,
                            timeout=7)
        self.__cookies['mrcu'] = resp.cookies['mrcu']
        html = resp.content.decode('utf-8')
        if 'id="noPhoneLink"' not in html:
            self.status = 1
        else:
            (self.sig1, self.x_reg_id, self.sig_year, self.sig_day, self.sig_name, self.sig_last_name, self.sig_sex,
             self.sig_mailbox, self.sig_password, self.sig_password_retype, self.sig_alt_email, self.sig_captcha,
             self.captcha_url, self.reg_id) = self.parse_reg_page(html)
            self.password = ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])
            self.first_name = f_name
            self.last_name = l_name
            self.sex = sex
            self.birth_year = random.randint(1970, 1990)
            self.birth_month = random.randint(1, 12)
            self.birth_day = random.randint(1, 28)
            self.captcha = requests.get(self.captcha_url, headers=self.__headers, cookies=self.__cookies,
                                        proxies=self.__proxies, timeout=10).content
            self.status = 0

    def get_email_list(self):
        post_data = {'RegistrationDomain': 'mail.ru', 'Signup_utf8': '1', 'LANG': 'en_US', self.sig1: '',
                     'x_reg_id': self.x_reg_id, self.sig_year: self.birth_year, 'BirthMonth': self.birth_month,
                     self.sig_day: self.birth_day, self.sig_name: self.first_name, self.sig_last_name: self.last_name,
                     self.sig_sex: self.sex}
        resp = requests.post('https://e.mail.ru/cgi-bin/checklogin', data=post_data, headers=self.__headers,
                             cookies=self.__cookies, proxies=self.__proxies, timeout=7).content.decode('utf-8')
        if '@' in resp:
            emails = resp.split('\n')[1:]
            split_emails = []
            for email in emails:
                pieces = email.split('@')
                split_emails.append([pieces[0], pieces[1]])
            return split_emails
        else:
            return False

    def register(self, captcha_key, email):
        self.__headers['Referer'] = 'https://e.mail.ru/signup?from=main_noc'
        register_data = {'signup_b': '1', 'sms': '1', 'no_mobile': '0', 'Signup_utf8': '1', 'LANG': 'en_US',
                         'ID': self.reg_id, 'Count': '1', 'back': '', 'Mrim.Country': '0', 'Mrim.Region': '0',
                         'x_reg_id': self.x_reg_id, 'security_image_id': '', 'geo_countryId': 'undefined',
                         'geo_cityId': 'undefined', 'geo_regionId': 'undefined', 'geo_country': '', 'geo_place': '',
                         'lang': 'en_US', 'new_captcha': '1', self.sig_name: self.first_name,
                         self.sig_last_name: self.last_name, self.sig_day: self.birth_day,
                         'BirthMonth': self.birth_month, self.sig_year: self.birth_year, self.sig_mailbox: email[0],
                         'RegistrationDomain': email[1], self.sig_password: self.password,
                         self.sig_password_retype: self.password, 'SelectPhoneCode': '7', 'RemindPhone': '',
                         'RemindPhoneCode': '7', self.sig_alt_email: '', self.sig_captcha: captcha_key,
                         self.sig_sex: self.sex}
        resp = requests.post('https://e.mail.ru/reg?from=main_noc', data=register_data, headers=self.__headers,
                             cookies=self.__cookies, proxies=self.__proxies, allow_redirects=False, timeout=7)
        html = resp.content.decode('utf-8')
        if 'The code was entered incorrectly.' in html:
            (self.sig1, self.x_reg_id, self.sig_year, self.sig_day, self.sig_name, self.sig_last_name, self.sig_sex,
             self.sig_mailbox, self.sig_password, self.sig_password_retype, self.sig_alt_email, self.sig_captcha,
             self.captcha_url, self.reg_id) = self.parse_reg_page(html)
            self.captcha = requests.get(self.captcha_url, headers=self.__headers, cookies=self.__cookies,
                                        proxies=self.__proxies).content
            return False, 1
        elif 'Location' in resp.headers:
            redirect_url = resp.headers['Location']
            requests.get(redirect_url, headers=self.__headers, cookies=self.__cookies, proxies=self.__proxies,
                         timeout=7)
            return True, email[0] + '@' + email[1], self.password
        elif 'Too many accounts have been registered from your IP address. Please try to sign up later.' in html:
            return False, 2
        else:
            with open('error.html', 'wb') as f:
                f.write(resp.content)
            return False, 0

    @staticmethod
    def parse_reg_page(html_f):
        marker1 = '''qc-login-row " onclick="return {'type': 'Login'};">'''
        marker2 = '" class="flr years mt0 mb0 qc-select-year"'
        marker3 = '<input type="hidden" name="x_reg_id" value="'
        marker4 = '" class="fll days mt0 mb0 qc-select-day"'
        marker5 = '<label class="sig1" for="'
        marker6 = 'qc-lastname-row " onclick="return {};">'
        marker7 = '<input type="radio" class="vtm" name="'
        marker9 = '''qc-login-row " onclick="return {'type': 'Login'}'''
        marker10 = '''qc-pass-row " onclick="return {'type': 'Password'};"'''
        marker11 = 'qc-passverify-row " id="signRePassword"'
        marker12 = '<input type="hidden" name="ID" value="'
        marker13 = '''qc-mail-row" onclick="return {'type': 'Email', 'noreq': true}'''
        marker14 = 'class="inPut form__captcha-old__input" type="text" name="'
        m_select = '<select name="'
        m_label = '<label for="'
        sig1_f = html_f.split(marker1)[1].split(m_label)[1].split('"')[0]
        sig_year_f = html_f.split(marker2)[0].split(m_select)[-1]
        x_reg_id_f = html_f.split(marker3)[1].split('"')[0]
        sig_day_f = html_f.split(marker4)[0].split(m_select)[-1]
        sig_name_f = html_f.split(marker5)[1].split('"')[0]
        sig_last_name_f = html_f.split(marker6)[1].split(m_label)[1].split('"')[0]
        sig_sex_f = html_f.split(marker7)[1].split('"')[0]
        sig_mailbox_f = html_f.split(marker9)[1].split(m_label)[1].split('"')[0]
        sig_password_f = html_f.split(marker10)[1].split(m_label)[1].split('"')[0]
        sig_password_retype_f = html_f.split(marker11)[1].split(m_label)[1].split('"')[0]
        sig_alt_email_f = html_f.split(marker13)[1].split(m_label)[1].split('"')[0]
        sig_captcha_f = html_f.split(marker14)[1].split('"')[0]
        captcha_url_f = 'https://c.mail.ru/' + html_f.split('<img src="//c.mail.ru/')[1].split('"')[0]
        reg_id_f = html_f.split(marker12)[1].split('"')[0]
        return (sig1_f, x_reg_id_f, sig_year_f, sig_day_f, sig_name_f, sig_last_name_f, sig_sex_f, sig_mailbox_f,
                sig_password_f, sig_password_retype_f, sig_alt_email_f, sig_captcha_f, captcha_url_f, reg_id_f)
