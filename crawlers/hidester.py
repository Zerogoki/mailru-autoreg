import requests


def crawler():
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'DNT': '1',
               'Upgrade-Insecure-Requests': '1', 'Referer': 'https://hidester.com/proxylist/'}
    json_proxies = requests.get('https://hidester.com/proxydata/php/data.php?mykey=data&offset=0&limit=400&orderBy=l'
                                'atest_check&sortOrder=DESC&country=&port=&type=undefined&anonymity=undefined&ping=u'
                                'ndefined&gproxy=2', headers=headers).json()
    proxies = []
    count = 0
    for p in json_proxies:
        if p['anonymity'] != 'Transparent' and p['ping'] < 600:
            proxies.append([p['IP'], p['PORT'], p['type']])
            count += 1
    return proxies, count
