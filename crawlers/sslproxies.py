import requests


def crawler():
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'DNT': '1',
               'Upgrade-Insecure-Requests': '1'}
    td_c = len("<td class='hm'>")
    td = len('<td>')
    html = requests.get('https://www.sslproxies.org/', headers=headers).content.decode('utf-8')
    table = html.split('<tbody>')[1].split('</tbody>')[0]
    entries = table.split('<tr>')[1:]
    i = 0
    proxy = []
    for entry in entries:
        data_pieces = entry.split('</td>')
        ip = data_pieces[0][td:]
        port = data_pieces[1][td:]
        anon = data_pieces[4][td:]
        ssl = data_pieces[6][td_c:]
        if ssl == 'yes' and anon != 'transparent':
            proxy.append([ip, port, 'http'])
            i += 1
    return proxy, i
