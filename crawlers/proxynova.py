import requests


def crawler():
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'DNT': '1',
               'Upgrade-Insecure-Requests': '1'}
    html = requests.get('https://www.proxynova.com/proxy-server-list/', headers=headers).content.decode('utf-8')
    table = html.split('<tbody>')[1].split('</tbody>')[0].split('</tr>')
    i = 0
    proxy = []
    for elem in table:
        if 'document.write' in elem:
            ip_p1 = elem.split("document.write(\'")[1][2:].split("\'")[0]
            ip_p2 = elem.split("substr(2) + \'")[1].split("\'")[0]
            if 'title="Proxy Port ' in elem:
                port = elem.split('title="Proxy Port ')[1].split('">')[0]
            else:
                port = elem.split('<td align="left">')[2].split('<')[0].split()[0]
            if 'Transparent' not in elem:
                i += 1
                proxy.append([ip_p1 + ip_p2, port, 'http'])
    return proxy, i
