from utils.tasker import Tasks
from utils.proxy_manager import ProxyManager
import random
import argparse
import webbrowser
from time import sleep


parser = argparse.ArgumentParser(description='Mail.Ru account register')
parser.add_argument('mode', help='single or multi')
parser.add_argument('-p', '--proxy-file', help='proxy file')
parser.add_argument('-fn', '--first-name', help='SINGLE first name')
parser.add_argument('-ln', '--last-name', help='SINGLE: last name')
parser.add_argument('-s', '--sex', help='SINGLE: sex', type=int)
parser.add_argument('-mc', '--male-count', help='MULTI: male count', type=int)
parser.add_argument('-fc', '--female-count', help='MULTI: female count', type=int)
parser.add_argument('-o', '--outfile',  help='outfile')
args = parser.parse_args()

mode = args.mode
proxy_file = args.proxy_file
sex = args.sex
first_name = args.first_name
last_name = args.last_name
male_count = args.male_count
female_count = args.female_count
outfile = args.outfile

if mode not in ('single', 'multi'):
    print('Mode must be single or multi')
    exit()

if mode == 'single':
    if not first_name or not last_name:
        print('First name and last name are required')
        exit()
    if sex not in (0, 1):
        sex = 0
if mode == 'multi':
    if male_count:
        pass
    elif female_count:
        pass
    else:
        print('Male or female count must be specified')
        exit()

if not proxy_file:
    print('Please specify proxy file with -p option')
    exit()

proxy_manager = ProxyManager()
loaded = proxy_manager.read_from_file(proxy_file)
if not loaded:
    print('File does not exists')
    exit()
if loaded is 1:
    print('Proxy list is empty')
    exit()
proxy = proxy_manager.proxies
print('Loaded %d proxies' % proxy_manager.count)

tasks = Tasks(proxy)
if mode == 'single':
    tasks.add_task([first_name, last_name, sex])
elif mode == 'multi':
    male_names = open('./names/male.txt').read().split()
    female_names = open('./names/female.txt').read().split()
    male_last_names = open('./names/male_s.txt').read().split()
    female_last_names = open('./names/female_s.txt').read().split()
    if male_count:
        for i in range(male_count):
            tasks.add_task([random.choice(male_names), random.choice(male_last_names), 0])
    if female_count:
        for i in range(female_count):
            tasks.add_task([random.choice(female_names), random.choice(female_last_names), 1])


webbrowser.open_new_tab('./captcha.html')
sleep(1.5)
tasks.process()
registered = tasks.registered

if not registered:
    exit()
print('\nRegistered accounts:')
for elem in registered:
    print('%s %s' % (elem[0], elem[1]))

if outfile:
    string = ''
    for elem in registered:
        string += '%s %s\n' % (elem[0], elem[1])
    with open(outfile, 'a') as f:
        f.write(string)
    print('\nAccounts have been written to', outfile)

proxy_manager.write_to_file(proxy_file, proxies=proxy)
